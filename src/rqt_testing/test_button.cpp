#include "rqt_testing/test_button.h"

#include <pluginlib/class_list_macros.h>
#include <ros/master.h>
#include <QStringList>

namespace rqt_testing {
  

test_button::test_button()
  : rqt_gui_cpp::Plugin()
  , widget_(0)
{
  setObjectName("test_button");
}

void test_button::initPlugin(qt_gui_cpp::PluginContext& context)
{
  QStringList argv = context.argv();

  widget_ = new QWidget();
  ui_.setupUi(widget_);

  //adds number if more than 1 of this plugin?
  if (context.serialNumber() > 1)
  {
    widget_->setWindowTitle(widget_->windowTitle() + " (" + QString::number(context.serialNumber()) + ")");
  }
  context.addWidget(widget_);

  //ui_.image_frame->installEventFilter(this);

  connect(ui_.pushButton, SIGNAL(pressed()), this, SLOT(on_pushButton_clicked()));

}

bool test_button::eventFilter (QObject* watched, QEvent* event)
{
  return true;
}

void test_button::shutdownPlugin() 
{
  
}

  
void test_button::on_pushButton_clicked()
{
  this->ui_.pushButton->setText("Pressed");
  ROS_INFO("Pressed the button");
}
  

} //namespace rqt_testing

PLUGINLIB_EXPORT_CLASS(rqt_testing::test_button, rqt_gui_cpp::Plugin)
