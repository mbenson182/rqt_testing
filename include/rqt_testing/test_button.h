#ifndef rqt_testing_test_button_H
#define rqt_testing_test_button_H

#include <rqt_gui_cpp/plugin.h>
#include <rqt_testing/ui_test_button.h>

#include <QWidget>
#include <QString>

namespace rqt_testing {
  

class test_button 
  : public rqt_gui_cpp::Plugin
{
  Q_OBJECT

public:
  test_button();

  virtual void initPlugin(qt_gui_cpp::PluginContext& context);

  virtual bool eventFilter(QObject* watched, QEvent* event);

  virtual void shutdownPlugin();

private slots:
  void on_pushButton_clicked();

private:
  Ui::test_button ui_;
  QWidget* widget_;
};

} //namespace rqt_testing

#endif // rqt_testing_test_button_H
